/**
@license
Copyright 2018 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import {
  html,
  BaseElement,
  Foundation,
  Adapter,
  property,
  query,
  customElement
} from "../base/base-element";
import { MDCSelect } from "@material/select";
import { MDCSelectFoundation } from "@material/select/foundation";
import { style } from "./mwc-select-css";

import "../line-ripple/mwc-line-ripple";
import "../floating-label/mwc-floating-label";

declare global {
  interface HTMLElementTagNameMap {
    "mwc-select": Select;
  }
}

export interface SelectFoundation extends Foundation {
  isChecked(): boolean;
  setChecked(value: boolean): void;
  setDisabled(disabled: boolean): void;
  handleAnimationEnd(): void;
  handleChange(): void;
}

export declare var SelectFoundation: {
  prototype: SelectFoundation;
  new (adapter: Adapter): SelectFoundation;
}

@customElement("mwc-select" as any)
export class Select extends BaseElement {
  private mdcComponent?: MDCSelect;
  @query(".mdc-select")
  protected mdcRoot!: HTMLElement;

  @query("#native_select")
  protected nativeSelect!: HTMLSelectElement;

  protected mdcFoundation!: SelectFoundation;

  protected readonly mdcFoundationClass: typeof SelectFoundation = MDCSelectFoundation;

  @property({ type: String })
  label = "";

  protected createAdapter() {
    return {
      ...super.createAdapter(),
      registerEventHandler: (type: string, handler: EventListener) => {
        this.mdcRoot.addEventListener(type, handler);
      },
      deregisterEventHandler: (type: string, handler: EventListener) => {
        this.mdcRoot.removeEventListener(type, handler);
      },
      activateBottomLine: () => {},
      deactivateBottomLine: () => {},
      setValue: (value: string) => {
        this.nativeSelect.value = value;
      },
      getValue: () => {
        return this.nativeSelect.value;
      },
      floatLabel: (value: boolean) => {},
      getLabelWidth: () => {},
      hasOutline: () => false,
      notchOutline: (labelWidth: number) => {},
      closeOutline: () => {},
      openMenu: () => {},
      closeMenu: () => {},
      isMenuOpen: () => {},
      setSelectedIndex: () => {},
      setDisabled: () => {},
      setRippleCenter: () => {},
      notifyChange: () => {},
      checkValidity: () => {},
      setValid: () => {}
    };
  }

  renderStyle() {
    return style;
  }

  firstUpdated() {
    this.mdcComponent = new MDCSelect()    
  }

  render() {
    const { label } = this;
    return html`
      ${this.renderStyle()}
      <div class="mdc-select">
        <i class="mdc-select__dropdown-icon"></i>
        <select id="native_select" name="native_select" class="mdc-select__native-control">
          <slot></slot>
        </select>
        <label class="mdc-floating-label" for="native_select">${label}</label>
        <div class="mdc-line-ripple"></div>
      </div>
    `;
  }
}

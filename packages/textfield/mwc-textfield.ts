/**
@license
Copyright 2018 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import {
  html,
  BaseElement,
  Foundation,
  Adapter,
  property,
  query,
  observer
} from "../base/base-element";
import { FormElement } from "../base/form-element";
import { classMap } from "lit-html/directives/class-map";
import { findAssignedElement } from "../base/utils";
import MDCTextFieldFoundation from "@material/textfield/foundation";
import { style } from "./mwc-textfield-css";
import "@material/mwc-icon/mwc-icon-font.js";

export interface TextFieldFoundation extends Foundation {}

export declare var TextFieldFoundation: {
  prototype: TextFieldFoundation;
  new (adapter: Adapter): TextFieldFoundation;
};

declare global {
  interface HTMLElementTagNameMap {
    "mwc-textfield": Textfield;
  }
}

export class Textfield extends BaseElement {
  protected createAdapter():Adapter {
    return {
      ...super.createAdapter(),
      /**
       * Registers an event handler on the root element for a given event.
       * @param {string} type
       * @param {function(!Event): undefined} handler
       */
      registerTextFieldInteractionHandler: (type: string,handler: EventListener) => {
        this.mdcRoot.addEventListener(type, handler);
      },
      /**
       * Deregisters an event handler on the root element for a given event.
       * @param {string} type
       * @param {function(!Event): undefined} handler
       */
      deregisterTextFieldInteractionHandler: (type: string,handler: EventListener) => {
        this.mdcRoot.removeEventListener(type, handler);
      },
      activateInputRipple: () => {
        const input = this.input;
        if (input instanceof FormElement && input.ripple) {
          input.ripple.activate();
        }
      },
      deactivateInputRipple: () => {
        const input = this.input;
        if (input instanceof FormElement && input.ripple) {
          input.ripple.deactivate();
        }
      },
      /**
       * Registers an event listener on the native input element for a given event.
       * @param {string} evtType
       * @param {function(!Event): undefined} handler
       */
      registerInputInteractionHandler: (evtType:string, handler: EventListener) => {
        const input = this.input;
        if (input instanceof HTMLInputElement){
          input.addEventListener(evtType, handler)
        }
      },

      /**
       * Deregisters an event listener on the native input element for a given event.
       * @param {string} evtType
       * @param {function(!Event): undefined} handler
       */
      deregisterInputInteractionHandler: (evtType:string, handler: EventListener) => {
        const input = this.input;
        if (input instanceof HTMLInputElement){
          input.removeEventListener(evtType, handler)
        }
      },

      /**
       * Registers a validation attribute change listener on the input element.
       * Handler accepts list of attribute names.
       * @param {(string[])=>void} handler
       * @return {!MutationObserver}
       */
      registerValidationAttributeChangeHandler: (handler: (string[]):void => void) => {},

      /**
       * Disconnects a validation attribute observer on the input element.
       * @param {!MutationObserver} observer
       */
      deregisterValidationAttributeChangeHandler(observer) {},

      /**
       * Returns an object representing the native text input element, with a
       * similar API shape. The object returned should include the value, disabled
       * and badInput properties, as well as the checkValidity() function. We never
       * alter the value within our code, however we do update the disabled
       * property, so if you choose to duck-type the return value for this method
       * in your implementation it's important to keep this in mind. Also note that
       * this method can return null, which the foundation will handle gracefully.
       * @return {?Element|?NativeInputType}
       */
      getNativeInput:() => this.input,

      /**
       * Returns true if the textfield is focused.
       * We achieve this via `document.activeElement === this.root_`.
       * @return {boolean}
       */
      isFocused:() => document.activeElement === this.mdcRoot,

      /**
       * Activates the line ripple.
       */
      activateLineRipple() {},

      /**
       * Deactivates the line ripple.
       */
      deactivateLineRipple() {},

      /**
       * Sets the transform origin of the line ripple.
       * @param {number} normalizedX
       */
      setLineRippleTransformOrigin(normalizedX) {},

      /**
       * Only implement if label exists.
       * Shakes label if shouldShake is true.
       * @param {boolean} shouldShake
       */
      shakeLabel(shouldShake) {},

      /**
       * Only implement if label exists.
       * Floats the label above the input element if shouldFloat is true.
       * @param {boolean} shouldFloat
       */
      floatLabel: (shouldFloat:boolean) => {},

      /**
       * Returns true if label element exists, false if it doesn't.
       * @return {boolean}
       */
      hasLabel: () => !!this.label,

      /**
       * Only implement if label exists.
       * Returns width of label in pixels.
       * @return {number}
       */
      getLabelWidth() {},

      /**
       * Returns true if outline element exists, false if it doesn't.
       * @return {boolean}
       */
      hasOutline() {},

      /**
       * Only implement if outline element exists.
       * @param {number} labelWidth
       */
      notchOutline(labelWidth) {},

      /**
       * Only implement if outline element exists.
       * Closes notch in outline element.
       */
      closeOutline() {}
    };
  }

  @property({ type: Boolean })
  public required: boolean = false;

  @property({ type: String })
  public value: string = "";

  @property({ type: String })
  @observer(async function(this: Textfield, label: string) {
    const input = this.input;
    if (input) {
      if (input.localName === "input") {
        input.setAttribute("aria-label", label);
      } else if (input instanceof FormElement) {
        await input.updateComplete;
        input.setAriaLabel(label);
      }
    }
  })
  public label: string = "";

  @property({ type: String })
  public icon: string = "";

  @property({ type: Boolean })
  public iconTrailing: boolean = false;

  @property({ type: String })
  public helperText: string = "";

  @property({ type: Boolean })
  public box: boolean = false;

  @property({ type: Boolean })
  public outlined: boolean = false;

  @property({ type: Boolean })
  public disabled: boolean = false;

  @property({ type: Boolean })
  public fullWidth: boolean = false;

  @property({ type: String })
  public placeHolder: string = "";

  @property({ type: String })
  public type: string = "";

  @query("slot")
  protected slotEl!: HTMLSlotElement;

  @query("label")
  protected labelEl!: HTMLLabelElement;

  @query(".mdc-text-field")
  protected mdcRoot!: HTMLElement;

  private _component: any;

  protected mdcFoundation!: TextFieldFoundation;

  protected readonly mdcFoundationClass: typeof TextFieldFoundation = MDCTextFieldFoundation;

  protected get input():HTMLInputElement | null {
    return findAssignedElement(this.slotEl, "*");
  }

  renderStyle() {
    return style;
  }

  // TODO(sorvell) #css: styling for fullwidth
  render() {
    const {
      value,
      label,
      box,
      outlined,
      disabled,
      icon,
      iconTrailing,
      fullWidth,
      required,
      placeHolder,
      helperText,
      type
    } = this;
    const hostClassInfo = {
      "mdc-text-field--with-leading-icon": icon && !iconTrailing,
      "mdc-text-field--with-trailing-icon": icon && iconTrailing,
      "mdc-text-field--box": !fullWidth && box,
      "mdc-text-field--outlined": !fullWidth && outlined,
      "mdc-text-field--disabled": disabled,
      "mdc-text-field--fullwidth": fullWidth
    };
    return html`
      ${this.renderStyle()}
      <div
        class="mdc-text-field mdc-text-field--upgraded ${
          classMap(hostClassInfo)
        }"
      >
        ${
          !fullWidth && icon
            ? html`
                <i class="material-icons mdc-text-field__icon" tabindex="0"
                  >${icon}</i
                >
              `
            : ""
        }
        ${this.renderInput({ value, required, type, placeHolder, label })}
        ${
          !fullWidth && label
            ? html`
                <label
                  class="mdc-floating-label ${
                    value ? "mdc-floating-label--float-above" : ""
                  }"
                  for="text-field"
                  >${label}</label>`
            : ""
        }
        ${
          !fullWidth && outlined
            ? html`
                <div class="mdc-notched-outline">
                  <svg><path class="mdc-notched-outline__path" /></svg>
                </div>
                <div class="mdc-notched-outline__idle"></div>
              `
            : html`
                <div class="mdc-line-ripple"></div>
              `
        }
      </div>
      ${
        helperText
          ? html`
              <p class="mdc-text-field-helper-text" aria-hidden="true">
                ${helperText}
              </p>
            `
          : ""
      }
    `;
  }

  private renderInput({ value, required, type, placeHolder, label }) {
    return html`
      <input type="${type}"
        placeholder="${placeHolder}"
        ?required="${required}"
        class="mdc-text-field__input ${value ? "mdc-text-field--upgraded" : ""}"
        id="text-field"
        .value="${value}"
        aria-label="${label}"
      />
    `;
  }

  get valid() {
    return this._component && this._component.isValid();
  }

  set valid(value) {
    this.componentReady().then(component => {
      component.setValid(value);
    });
  }

  click() {
    const input = this.input;
    if (input) {
      input.focus();
      input.click();
    }
  }

  focus() {
    const input = this.input;
    if (input) {
      input.focus();
    }
  }
}

customElements.define("mwc-textfield", Textfield);

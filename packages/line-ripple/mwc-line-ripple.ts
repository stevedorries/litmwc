/**
@license
Copyright 2018 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import {html, BaseElement, Foundation, Adapter, query, customElement} from '../base/base-element';
import {style} from './mwc-line-ripple-css';
import MDCLineRippleFoundation from '@material/line-ripple/foundation';

export interface LineRippleFoundation extends Foundation {
}

export declare var LineRippleFoundation: {
  prototype: LineRippleFoundation;
  new(adapter: Adapter): LineRippleFoundation;
}

declare global {
  interface HTMLElementTagNameMap {
    'mwc-lineripple': LineRipple;
  }
}

@customElement('mwc-line-ripple' as any)
export class LineRipple extends BaseElement {  
  @query('.mdc-line-ripple')
  protected mdcRoot!: HTMLElement;

  protected mdcFoundation!: LineRippleFoundation;

  protected readonly mdcFoundationClass: typeof LineRippleFoundation = MDCLineRippleFoundation;

  protected createAdapter() {
    return {
      ...super.createAdapter(),
      registerEventHandler: (type: string, handler: EventListener) => {
        this.mdcRoot.addEventListener(type, handler);
      },
      deregisterEventHandler: (type: string, handler: EventListener) => {
        this.mdcRoot.removeEventListener(type, handler);
      }
    }
  }

  renderStyle() {
    return style;
  }

  render() {
    return html`${this.renderStyle()}
      <div class="mdc-line-ripple"></div>`;
  }

}